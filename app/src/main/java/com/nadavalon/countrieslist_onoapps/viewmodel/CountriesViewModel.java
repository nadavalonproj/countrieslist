package com.nadavalon.countrieslist_onoapps.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


import com.nadavalon.countrieslist_onoapps.model.Country;
import com.nadavalon.countrieslist_onoapps.network.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CountriesViewModel extends ViewModel {
    private MutableLiveData<List<Country>> countryList;


    public LiveData<List<Country>> getCountries() {
        if (countryList == null) {
            countryList = new MutableLiveData<List<Country>>();
            loadCountries();
        }
        return countryList;
    }

    private void loadCountries() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<Country>> call = api.getCountries();


        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                countryList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
            }
        });
    }
}