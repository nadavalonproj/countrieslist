package com.nadavalon.countrieslist_onoapps.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nadavalon.countrieslist_onoapps.R;
import com.nadavalon.countrieslist_onoapps.model.Country;

import java.util.List;

public class CountryActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CountryAdapter adapter;
    List<String> borders;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Bundle data = getIntent().getExtras();
        Country country = data.getParcelable("country");

        borders = country.getBorders();
        adapter = new CountryAdapter(CountryActivity.this, borders);
        recyclerView.setAdapter(adapter);
    }
}
