package com.nadavalon.countrieslist_onoapps.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.nadavalon.countrieslist_onoapps.R;
import com.nadavalon.countrieslist_onoapps.model.Country;
import com.nadavalon.countrieslist_onoapps.viewmodel.CountriesViewModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    CountriesViewModel model;
    RecyclerView recyclerView;
    CountriesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        model = ViewModelProviders.of(this).get(CountriesViewModel.class);

        model.getCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(@Nullable List<Country> countriesList) {
                recyclerView.setAdapter(adapter = new CountriesAdapter(MainActivity.this, countriesList, new CountriesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Country item) {
                        Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                        intent.putExtra("country", new Country(item.getNativeName(), item.getName(), item.getArea(), item.getBorders()));
                        startActivity(intent);
                    }
                }
                ));
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sort, menu);
        return true;
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        model.getCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(@Nullable List<Country> countriesList) {
                switch (item.getItemId()) {
                    case R.id.name_asc:
                        Collections.sort(countriesList, new Comparator<Country>() {
                            @Override
                            public int compare(Country o1, Country o2) {
                                return o1.getName().compareToIgnoreCase(o2.getName());
                            }
                        });
                        break;
                    case R.id.name_dsc:
                        Collections.sort(countriesList, new Comparator<Country>() {
                            @Override
                            public int compare(Country o1, Country o2) {
                                return o2.getName().compareToIgnoreCase(o1.getName());
                            }
                        });
                        break;

                    case R.id.area_asc:
                        Collections.sort(countriesList, new Comparator<Country>() {
                            @Override
                            public int compare(Country o1, Country o2) {
                                if(o1.getArea() == null){
                                    return 1;
                                }
                                if(o2.getArea() == null){
                                    return -1;
                                }
                                return Double.compare(o1.getArea(),o2.getArea());
                            }
                        });
                        break;
                    case R.id.area_dsc:
                        Collections.sort(countriesList, new Comparator<Country>() {
                            @Override
                            public int compare(Country o1, Country o2) {
                                if(o1.getArea() == null){
                                    return 1;
                                }
                                if(o2.getArea() == null){
                                    return -1;
                                }
                                return Double.compare(o2.getArea(),o1.getArea());
                            }
                        });
                        break;
                }

                recyclerView.setAdapter(adapter = new CountriesAdapter(MainActivity.this, countriesList, new CountriesAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Country item) {
                        Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                        intent.putExtra("country", new Country(item.getNativeName(), item.getName(), item.getArea(), item.getBorders()));
                        startActivity(intent);
                    }
                }
                ));
            }
        });
        return super.onOptionsItemSelected(item);
    }
}
