package com.nadavalon.countrieslist_onoapps.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.nadavalon.countrieslist_onoapps.R;
import com.nadavalon.countrieslist_onoapps.model.Country;

import java.util.List;
public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {
    Context mCtx;
    List<String> borders;
    public CountryAdapter(Context mCtx, List<String> borders)  {
        this.mCtx = mCtx;
        this.borders = borders;
    }

    @NonNull
    @Override
    public CountryAdapter.CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.borders_item, parent, false);
        return new CountryAdapter.CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryAdapter.CountryViewHolder holder, int position) {
        if(getItemCount() == 0){
            borders.add("1");
            holder.tvBorder.setText("No neighbor countries");
        }
        else {
            String border = borders.get(position).toString();
            holder.tvBorder.setText(border);
        }
    }

    @Override
    public int getItemCount() {
        return borders.size();
    }

    class CountryViewHolder extends RecyclerView.ViewHolder {
        TextView tvBorder;


        public CountryViewHolder(View itemView) {
            super(itemView);

            tvBorder = itemView.findViewById(R.id.tv_border);
        }
    }
}
