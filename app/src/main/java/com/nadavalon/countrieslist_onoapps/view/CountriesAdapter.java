package com.nadavalon.countrieslist_onoapps.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.nadavalon.countrieslist_onoapps.R;
import com.nadavalon.countrieslist_onoapps.model.Country;

import java.util.List;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.CountryViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Country item);
    }

    Context mCtx;
    List<Country> countriesList;

    //to provide onitemclicklistener to recyclerview
    private final OnItemClickListener listener;


    public CountriesAdapter(Context mCtx, List<Country> countriesList, OnItemClickListener listener) {
        this.mCtx = mCtx;
        this.countriesList = countriesList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.countries_item, parent, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryViewHolder holder, int position) {
        holder.bind(countriesList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return countriesList.size();
    }

    class CountryViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvNativeName;
        private TextView tvArea;


        public CountryViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvNativeName = itemView.findViewById(R.id.tv_native_name);
            tvArea = itemView.findViewById(R.id.tv_area);
        }

        public void bind(final Country country, final OnItemClickListener listener) {
            tvName.setText(country.getName());
            tvNativeName.setText(country.getNativeName());
            tvArea.setText(country.getArea().toString());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(country);
                }
            });
        }
    }
}